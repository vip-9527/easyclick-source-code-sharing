/**
 * @author Mr_老冷 QQ1920712147
 * @description 判断数组是否不为空
 * @param arr 数组
 * @return {boolean}
 */
function isNotEmptyArray(arr) {
    if (!arr) return false
    return arr.length !== 0;
}

let res = getLastNotification("com.x", 100)
if (isNotEmptyArray(res)) {
    for (let i = 0; i < res.length; i++) {
        logd(res[i]);
    }
}