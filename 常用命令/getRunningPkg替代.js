/**
 * @author Mr_老冷 QQ:1920712147
 * @description getRunningPkg替代
 * @return {null|*|string}
 */
function getRunningPkgEx() {
    let node = bounds(100, 200, device.getScreenWidth(), device.getScreenHeight() - 100).getOneNodeInfo(0)
    return node ? node.pkg : ""
}