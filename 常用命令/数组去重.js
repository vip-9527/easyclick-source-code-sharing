 方法一:原型方法
/**
 * @author Mr_老冷 QQ:1920712147
 * @date 20210722
 */
Array.prototype.unique = function () {
    let tmpS = new Set(this), i = 0
    for (let tmp of tmpS) {
        this[i] = tmp
        i++
    }
    this.length = i
}
let a = [1, 2, 2, 3, 4]
a.unique()
console.log(a)
//[1,2,3,4]


方法二:函数方法
/**
 * @description 数组去重,函数方法
 * @param arr {any[]} 待去重数组
 * @return {any[]} 去重后数组
 */
function arrUnique(arr) {
    return Array.from(new Set(arr))
}
let a = [1, 2, 2, 3, 3]
a = arrUnique(a)
console.log(a)
//[1,2,3]