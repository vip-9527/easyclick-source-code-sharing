/**
 * @author Mr_老冷 QQ1920712147
 * @description 包名是否在前台
 * @param pkgName 包名
 * @return {boolean} true/false
 */
isRunningPkg = function (pkgName) {
    return !!pkg(pkgName).getOneNodeInfo(0)
}