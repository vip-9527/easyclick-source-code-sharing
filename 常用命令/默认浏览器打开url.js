/**
* @author Mr_老冷 QQ:1920712147
 *@description 默认浏览器打开网址
 * @param {string} url 网址链接
 * @return {boolean}
 */
function openUrl(url) {
    utils.openActivity({
        action: "android.intent.action.VIEW",
        uri: url,
    })
}