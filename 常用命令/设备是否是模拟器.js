/**
 * @description 设备是否是模拟器
 * @return {Boolean} true 模拟器 false 非模拟器
 */
isSimulator = function () {
    let ret = android.os.Build.SUPPORTED_ABIS
    for (let v of ret) {
        if (v.indexOf("x86") > -1) {
            return true
        }
    }
    return false
}